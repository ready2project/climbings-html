(function () {
  'use strict';

  var svgTest = function svgTest() {
    /**
     * SVG Support
     */
    var svgSupport = !!document.createElementNS && !!document.createElementNS('http://www.w3.org/2000/svg', 'svg').createSVGRect;

    if (!svgSupport) {
      document.body.classList.add('svg-unsupported');
    }
  };

  /**
   * Component: Sticky Slider
   */
  var initStickySlider = function initStickySlider() {
    /**
     * SVG Support
     */
    var svgSupport = !!document.createElementNS && !!document.createElementNS('http://www.w3.org/2000/svg', 'svg').createSVGRect;

    if (!svgSupport) {
      document.body.classList.add('svg-unsupported');
    }

    var screenWidth = window.innerWidth;
    var sliderBreakPoint = 1120;

    if (screenWidth > sliderBreakPoint) {
      // Indicator Items
      var sliderLinks = document.querySelectorAll('.sticky-slider__link');
      var sliderNavTitle = document.querySelector('.sticky-slider-nav__title'); // Areas

      var areas = document.querySelectorAll('.area');
      var areasLength = areas.length;
      var areasParallax = document.querySelectorAll('.area-parallax');
      var areasParallaxLength = areasParallax.length;

      if (sliderLinks && sliderNavTitle && areas) {
        var sliderLinksLength;
        var i;
        var slider;
        var sliderLinks;

        (function () {
          sliderLinksLength = sliderLinks.length;

          var _loop = function _loop() {
            /**
             * Event: Mouse Hover
             */
            sliderLinks[i].addEventListener('mousemove', function (e) {
              var obj = e.currentTarget;
              var targetTitle = obj.text;
              sliderNavTitle.innerHTML = targetTitle;
              var elActive = document.querySelector('.sticky-slider__link.active');

              if (elActive) {
                elActive.classList.remove('active');
                elActive.classList.add('outfocus');
              }
            });
            /**
             * Event: Mouse Out. Recovery Active Element Status
             */

            sliderLinks[i].addEventListener('mouseout', function (e) {
              var elActive = document.querySelector('.sticky-slider__link.outfocus');

              if (elActive) {
                elActive.classList.remove('outfocus');
                elActive.classList.add('active');
                var targetTitle = elActive.text;
                sliderNavTitle.innerHTML = targetTitle;
              }
            });
            /**
             * Scroll
             *
             * @param {Element} el
             */

            function elScroll(el) {
              if (el) {
                el.scrollIntoView({
                  behavior: 'smooth',
                  block: 'start',
                  inline: 'start'
                });
              }
            }
            /**
             * Event: Mouse Click
             */


            sliderLinks[i].addEventListener('click', function (e) {
              e.preventDefault();
              var obj = e.currentTarget; // Target Area

              var targetArea = obj.dataset.target;
              var targetTitle = obj.text;
              var el = document.getElementById(targetArea);
              sliderNavTitle.innerHTML = targetTitle;
              [].forEach.call(sliderLinks, function (el) {
                el.classList.remove('outfocus');
              });
              obj.classList.add('outfocus');

              if (areasParallax) {
                var fixedAreas = document.querySelectorAll('.area-parallax.fixed');

                if (fixedAreas) {
                  for (var id = 0; id < areasParallaxLength; id++) {
                    areasParallax[id].classList.remove('fixed');
                  }

                  window.scrollTo(0, 0);
                  setTimeout(function () {
                    elScroll(el);
                    sliderScroll();
                  }, 50);
                } else {
                  elScroll(el);
                }
              } else {
                elScroll(el);
              }
            });
          };

          for (i = 0; i < sliderLinksLength; i++) {
            _loop();
          }
          /**
           * Event: Scroll
           */


          slider = document.getElementById('stickySlider');
          sliderLinks = document.querySelectorAll('.sticky-slider__link');

          function sliderScroll() {
            // Indicator Nav Position
            var fixed_position = slider.getBoundingClientRect().top + document.body.scrollTop;
            var fixed_height = getClientHeight(slider);

            if (areasLength > 0) {
              for (var i = 0; i < areasLength; i++) {
                var elCross = areas[i];
                var toCross_position = elCross.getBoundingClientRect().top + document.body.scrollTop;
                var toCross_height = getClientHeight(elCross);

                if (fixed_position + fixed_height < toCross_position) ; else if (fixed_position > toCross_position + toCross_height) ; else {
                  var sliderLink = sliderLinks[i];
                  [].forEach.call(sliderLinks, function (el) {
                    el.classList.remove('active');
                  });
                  sliderLink.classList.add('active'); // In Section

                  var targetTitle = sliderLink.text;
                  sliderNavTitle.innerHTML = targetTitle;

                  if (elCross.classList.contains('area-inverted')) {
                    slider.classList.add('inverted');
                  } else {
                    slider.classList.remove('inverted');
                  }
                }
              }
            }
          }

          sliderScroll();
          window.addEventListener('scroll', function (e) {
            sliderScroll();
          });
        })();
      } // if isset

    } // if screen

  };

  /**
   * Load methods, helpers, polyfills etc.
   */
  /**
   * The DOMContentLoaded event fires when the initial HTML document has been completely loaded and parsed,
   * without waiting for stylesheets, images, and subframes to finish loading.
   *
   * https://developer.mozilla.org/en-US/docs/Web/API/Window/DOMContentLoaded_event
   */

  document.addEventListener('DOMContentLoaded', function () {
    // Test SVG Support
    svgTest(); // Sticky Slider

    initStickySlider();
  });

}());
//# sourceMappingURL=app.js.map
