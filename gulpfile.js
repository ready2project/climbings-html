(() => {

  'use strict';

  const {
    src,
    dest,
    parallel,
    series
  } = require('gulp');

  /**
   * Modules
   */
  const concat = require('gulp-concat');
  const cssmin = require('gulp-clean-css');
  const minify = require('gulp-minify');
  //const purgecss = require('gulp-purgecss')
  const rename = require('gulp-rename');
  const replace = require('gulp-replace');
  const fs = require('fs');
  const obfuscator = require('gulp-javascript-obfuscator');


  /**
   * Scripts
   */
  function scripts() {
    return src([
      './javascript/keyshapejs.min.js',
      './javascript/helpers.js',
      './javascript/nav.js',
      './javascript/component-sticky-slider.js',
      './javascript/services-tabs.js',
      './javascript/our-process-tabs.js',
      './javascript/scene-cta.js',
      './javascript/scene-process.js',
      './javascript/scene-intro.js',
      './javascript/slider-works.js',
      './javascript/slider-testimonials.js',
      './javascript/home-parallax.js',
      './javascript/app.js'
    ])
      .pipe(concat('app.js'))
      .pipe(minify())
      .pipe(dest('./build/javascript/'));
  }


  /**
   * Styles
   */
  function styles() {
    return src([
      './styles/main.css',
    ])
      .pipe(cssmin({
        compatibility: 'ie10'
      }))
      .pipe(dest('./build/styles/'));
  }


  /**
   * Purge Styles
   */
  function stylesPurge() {
    return src('./styles/main.css')
      .pipe(purgecss({
        content: [
          '/home.html',
          'build/javascript/app.js'
        ]
      }))
      .pipe(dest('purge/styles'));
  }


  /**
   * Images
   */
  function images() {
    return src([
      './images/*',
    ])
      .pipe(dest('./build/images/'));
  }

  /**
   * Fonts
   */
  function fonts() {
    return src([
      './fonts/*',
    ])
      .pipe(dest('./build/fonts/'));
  }

  /**
   * Scripts Obfuscator
   */
  function obfuscate() {
    return src([
      './build/javascript/app-min.js',
    ])
      .pipe(obfuscator({
        compact: true
      }))
      .pipe(dest('./build/javascript/'));
  }


  /**
   * Default
   */
  exports.default = series(
    scripts,
    styles,
    images,
    fonts,
  );


})();