/**
 * Helpers
 */


/**
 * Get Page Height
 */
function getViewHeight() {
  return Math.max(document.documentElement.clientHeight, window.innerHeight);
}

/**
 * Check Element Visibility
 *
 * @param {Element} el
 */
function checkVisible(el, offset) {
  if (el) {

    if (isNaN(offset)) {
      offset = 0;
    }

    var rect = el.getBoundingClientRect();
    var viewHeight = getViewHeight();

    return !(rect.bottom < offset || rect.top - viewHeight >= offset);
  }
}


