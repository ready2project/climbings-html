/**
 * Navigation
 */

const initNav = () => {

  /**
   * Button Toggle
   */
  var btnNav = document.getElementById('btnNav');
  var navBar = document.querySelector('.nav');
  var header = document.querySelector('.header');
  var headerAnchor = header.querySelector('.container');

  if (btnNav) {
    btnNav.addEventListener('click', function (e) {
      e.stopPropagation();

      e.preventDefault();
      btnNav.classList.toggle('active');
      document.body.classList.toggle('main-menu-active');
    });
  }


  /**
   * Correct Nav Button Position
   */

  if (navBar) {

    var navBarStyle = navBar.style;

    function setNavBtn() {
      var rect = headerAnchor.getBoundingClientRect();

      navBarStyle.right = '';
      if (window.innerWidth > 1200) {
        navBarStyle.right = rect.left + 15 + 'px';
      }
    }

    if (headerAnchor) {
      setNavBtn();
    }

    window.addEventListener('resize', function () {
      setNavBtn();
    });

    navBarStyle.display = 'flex';
  }


  /**
   * Event: Scroll (Short Menu)
   */
  if (header && navBar) {

    function navScroll() {

      var docScrollTop = document.body.scrollTop;
      var fixed_position = navBar.getBoundingClientRect().top + docScrollTop;
      var fixed_height = navBar.clientHeight;

      var toCross_position = header.getBoundingClientRect().top + docScrollTop;
      var toCross_height = header.clientHeight;

      if (fixed_position + fixed_height < toCross_position) {
        navBar.classList.add('nav-short')
      } else if (fixed_position > toCross_position + toCross_height) {
        navBar.classList.add('nav-short')
      } else {
        navBar.classList.remove('nav-short')
      }
    }

    navScroll();

    window.addEventListener('scroll', function (e) {
      navScroll();
    });

    /**
     * Event: Keypress
     */
    document.onkeydown = function (e) {
      if (document.body.classList.contains('main-menu-active')) {

        var keyEsc = 27;

        switch (e.keyCode) {
          case keyEsc:
            e.preventDefault();

            btnNav.click();
            break;
        }
      }
    }

    /**
     * Event: Blur
     */
    window.addEventListener('click', function () {
      if (document.body.classList.contains('main-menu-active')) {
        btnNav.click();
      }
    });

    var mainMenu = document.getElementById('mainMenu');

    if (mainMenu) {
      mainMenu.addEventListener('click', function (e) {
        e.stopPropagation();
      });
    }

  } // end. if header & nav


  /**
   * Button 'Start a Project' behaviour
   */

  var areasRestrictedNavStart = document.querySelectorAll('.area-startbtn-hide, .header');
  var areasRestrictedNavStartLength = areasRestrictedNavStart.length;

  function btnNavStartScroll() {

    // Indicator Nav Position
    var fixed_position = navBar.getBoundingClientRect().top + document.body.scrollTop;
    var fixed_height = getClientHeight(navBar);

    if (areasRestrictedNavStartLength > 0) {
      for (var i = 0; i < areasRestrictedNavStartLength; i++) {
        var elCross = areasRestrictedNavStart[i];
        var toCross_position = elCross.getBoundingClientRect().top + document.body.scrollTop;
        var toCross_height = getClientHeight(elCross);

        if (fixed_position + fixed_height < toCross_position) {
          navBar.classList.add('nav-short');
        } else if (fixed_position > toCross_position + toCross_height) {
          navBar.classList.add('nav-short');
        } else {
          navBar.classList.remove('nav-short');
          break;
        }
      }
    }
  }

  btnNavStartScroll();

  window.addEventListener('scroll', function (e) {

    btnNavStartScroll();

  });


}
export default initNav;