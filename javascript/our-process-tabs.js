/**
 * Home: Services Tabs
 */


const initServiceTabs = () => {

  // Get tabs
  var tabsOurProcess = document.querySelectorAll('.our-process-menu__link');

  if (tabsOurProcess) {

    // Slider Process Title
    var processTitle = document.querySelector('.our-process-nav__title');

    /**
     * Show Slider Navigation Title
     * @param {int} id
     */
    function setProcessTitle(id) {
      processTitle.innerHTML = tabsOurProcess[id].innerHTML;
    }

    /**
     * Clear Tabs Active Status
     */
    function clearTabsStatus() {
      [].forEach.call(tabsOurProcess, function (el) {
        el.classList.remove('active');
      });
    }

    /**
     * Home: Our Process Slider
     */
    var sliderProcess = new Swiper('.our-process-slider', {
      loop: false,
      slidesPerView: 1,
      preloadImages: false,
      lazy: true,
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
      scrollbar: {
        el: '.swiper-scrollbar',
        hide: false,
        draggable: true,
        dragSize: 50
      }
    });

    /**
     * Slider Action
     */
    sliderProcess.on('slideChange', function () {
      var slideId = sliderProcess.activeIndex;

      // Clear Active Tab Status
      clearTabsStatus();

      // Active Tab
      tabsOurProcess[slideId].classList.add('active');

      setProcessTitle(slideId);
    });

    /**
     * Tabs Action
     */
    var tabsOurProcessLength = tabsOurProcess.length;
    for (var i = 0; i < tabsOurProcessLength; i++) {


      tabsOurProcess[i].addEventListener('click', function (e) {
        e.preventDefault();

        // Clear Active Tab Status
        clearTabsStatus();

        // Active Tab
        var obj = e.currentTarget;
        obj.classList.add('active');

        // Show Start Points
        var points = obj.dataset.index;

        // Slider
        sliderProcess.slideTo(points - 1);
        setProcessTitle(points - 1);

      });


    }
  }


}

export default initServiceTabs;