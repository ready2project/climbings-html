const sceneProcess = () => {

  if (!KeyshapeJS.version.indexOf('1.') != 0) {

    /**
     * Animation for Our Process Section
     */

    var sceneProcess = document.getElementById('sceneProcess');
    var sceneProcesslaunch = true;

    /**
     * Show Animation
     */
    function showProcessSceneAnimation() {
      var offset = -300;

      if (sceneProcesslaunch) {
        if (checkVisible(sceneProcess, offset)) {
          getProcessSceneAnimation();
          sceneProcesslaunch = false;
        }
      }
    }

    showProcessSceneAnimation();

    /**
     * Animation Frames
     */
    function getProcessSceneAnimation() {
      window.ks = document.ks = KeyshapeJS; (function (ks) {
        ks.animate("#Path", [{ p: 'strokeDasharray', t: [0, 10000], v: ['20', '0'], e: [[1, 0, 1, 1, 1], [0]] }],
          "#Flag---Design", [{ p: 'scaleX', t: [2000, 3000], v: [0, 1], e: [[1, 0, 2, 1, 1], [0]] }, { p: 'scaleY', t: [2000, 3000], v: [0, 1], e: [[1, 0, 2, 1, 1], [0]] }],
          "#Flag---Develop", [{ p: 'scaleX', t: [4000, 5000], v: [0, 1], e: [[1, 0, 2, 1, 1], [0]] }, { p: 'scaleY', t: [4000, 5000], v: [0, 1], e: [[1, 0, 2, 1, 1], [0]] }],
          "#Flag---Test", [{ p: 'scaleX', t: [6000, 7000], v: [0, 1], e: [[1, 0, 2, 1, 1], [0]] }, { p: 'scaleY', t: [6000, 7000], v: [0, 1], e: [[1, 0, 2, 1, 1], [0]] }],
          "#Flag---Discover", [{ p: 'scaleX', t: [0, 1000], v: [0, 1], e: [[1, 0, 2, 1, 1], [0]] }, { p: 'scaleY', t: [0, 1000], v: [0, 1], e: [[1, 0, 2, 1, 1], [0]] }],
          "#Design", [{ p: 'scaleX', t: [3000, 3500], v: [0, 1], e: [[1, 0, 2, 1, 1], [0]] }, { p: 'scaleY', t: [3000, 3500], v: [0, 1], e: [[1, 0, 2, 1, 1], [0]] }],
          "#Develop", [{ p: 'scaleX', t: [5000, 5500], v: [0, 1], e: [[1, 0, 2, 1, 1], [0]] }, { p: 'scaleY', t: [5000, 5500], v: [0, 1], e: [[1, 0, 2, 1, 1], [0]] }],
          "#Test", [{ p: 'scaleX', t: [7000, 7500], v: [0, 1], e: [[1, 0, 2, 1, 1], [0]] }, { p: 'scaleY', t: [7000, 7500], v: [0, 1], e: [[1, 0, 2, 1, 1], [0]] }],
          "#Discover-4", [{ p: 'scaleX', t: [1000, 1500], v: [0, 1], e: [[1, 0, 2, 1, 1], [0]] }, { p: 'scaleY', t: [1000, 1500], v: [0, 1], e: [[1, 0, 2, 1, 1], [0]] }],
          "#Launch", [{ p: 'scaleX', t: [8000, 9000], v: [0, 1], e: [[1, 0, 2, 1, 1], [0]] }, { p: 'scaleY', t: [8000, 9000], v: [0, 1], e: [[1, 0, 2, 1, 1], [0]] }],
          "#Climber", [{ p: 'mpath', t: [0, 2000, 4000, 6000, 8000], v: ['0%', '35.5%', '59.9%', '71.3%', '100%'], e: [[1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [1, 0, 1, 1, 1], [0]], mp: "M48.1,884.3C316.5,877,203.5,756.5,296.2,770C401,784,296,704.5,462,685.3C488.8,671.8,528.5,672.5,533,632C497.6,587.5,430.5,456,382.2,453" }],
          "#Rocket-Fire", [{ p: 'mpath', t: [0, 1000, 2000, 3000, 4000, 5000, 6000, 8000, 10000], v: ['0%', '3.7%', '8.8%', '14.4%', '18.8%', '23.2%', '27.6%', '31.9%', '100%'], e: [[0], [0], [0], [0], [0], [0], [0], [1, 0, 0, .6, 1], [0]], mp: "M379,380.6L379,363.6L379,386.6L379,360.6L379,380.6L379,360.6L379,380.6L379,360.6L379,49.6" }, { p: 'scaleX', t: [8000, 10000], v: [1, .7], e: [[0], [0]] }, { p: 'scaleY', t: [8000, 10000], v: [1, .7], e: [[0], [0]] }, { p: 'opacity', t: [9000, 10000], v: [1, 0], e: [[0], [0]] }],
          "#Rocket-Fire-2", [{ p: 'opacity', t: [10000, 11000], v: [0, 1], e: [[0], [0]] }],
          { autoremove: false }).range(0, 12000);
      })(KeyshapeJS);
    }



    /**
     * Check Visibility
     */
    window.addEventListener('scroll', function () {
      showProcessSceneAnimation();
    });


  }
}

export default sceneProcess;