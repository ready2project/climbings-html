/**
 * Home: Services Tabs
 */


const serviceTabs = () => {


  // Get all tabs
  var tabsServices = document.querySelectorAll('.services-tabs__link');
  var tabsServicesContent = document.querySelectorAll('.services-content__item');


  if (tabsServices) {

    var tabsServicesLength = tabsServices.length;
    for (var i = 0; i < tabsServicesLength; i++) {

      /**
       * Set Active Status
       */
      tabsServices[i].addEventListener('mousemove', function (e) {
        e.preventDefault();

        [].forEach.call(tabsServices, function (el) {
          el.classList.remove('active');
        });

        [].forEach.call(tabsServicesContent, function (el) {
          el.classList.remove('active');
        });

        var obj = e.currentTarget;
        obj.classList.add('active');

        var link = obj.hash;
        var actualTab = document.querySelector(link);
        actualTab.classList.add('active');

      });

      tabsServices[i].addEventListener('click', function (e) {
        e.preventDefault();
      });


    }
  }





}

export default serviceTabs;