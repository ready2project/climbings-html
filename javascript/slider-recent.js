/**
 * Home: Recent Slider
 */

const sliderRecent = () => {

  var recentSlider = document.querySelector('.recent-slider');
  if (recentSlider) {
    var swiperRecent = new Swiper('.recent-slider', {
      loop: true,
      variableWidth: true,
      slidesPerView: 1,
      spaceBetween: 20,
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
      breakpoints: {
        768: {
          slidesPerView: 2,
          spaceBetween: 30,
        },

        528: {
          slidesPerView: 2,
          spaceBetween: 20,
        },
      }
    });
  }


}

export default sliderRecent;