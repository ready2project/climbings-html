/**
 * Home: Testimonials Slider
 */

const sliderTestimonials = () => {

  var testimonialsSlider = document.querySelector('.testimonials-slider');
  if (testimonialsSlider) {
    var swiperTestimonials = new Swiper('.testimonials-slider', {
      loop: false,
      variableWidth: true,
      slidesPerView: 1,
      spaceBetween: 20,
      pagination: {
        el: '.swiper-pagination',
        type: 'fraction',
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
      scrollbar: {
        el: '.swiper-scrollbar',
        hide: false,
        draggable: true,
        dragSize: 50
      },
      breakpoints: {
        768: {
          slidesPerView: 3,
          spaceBetween: 30,
        },

        528: {
          slidesPerView: 2,
          spaceBetween: 20,
        },
      }
    });
  }

}

export default sliderTestimonials;