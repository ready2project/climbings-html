/**
 * Home: Works Slider
 */

const sliderWorks = () => {


  var worksSlider = document.querySelector('.works-slider');
  if (worksSlider) {
    var sliderWorks = new Swiper('.works-slider', {
      loop: true,
      centeredSlides: true,
      spaceBetween: 46,
      slidesPerView: 5,
      preloadImages: false,
      lazy: true,
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
      breakpoints: {
        768: {
          spaceBetween: 66,
        },
      }
    });
  }

}

export default sliderWorks;