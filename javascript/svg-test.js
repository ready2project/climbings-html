const svgTest = () => {

  /**
   * SVG Support
   */
  let svgSupport = !!document.createElementNS && !!document.createElementNS('http://www.w3.org/2000/svg', 'svg').createSVGRect;
  if (!svgSupport) {
    document.body.classList.add('svg-unsupported');
  }

}

export default svgTest;