Technical Information
-------------------------------

- dev. site: https://climbings-dev-html.netlify.com
- git repo HTML: https://bitbucket.org/ready2project/climbings-html/src/master/
- git repo WordPress: https://bitbucket.org/ready2project/climbings-wp/src/master/


External JS Libraries
===============================

- https://swiperjs.com/
- /javascript/keyshapejs.min.js


Components
-------------------------------


Sticky Sections Indicator
===============================

- javascript/component-sticky-slider.js
- scss/_component-sticky-slider.scss


Animation
-------------------------------

- /javascript/scene-cta.js - Animation for CTA Section
- /javascript/scene-process.js - Animation for Our Process Section
